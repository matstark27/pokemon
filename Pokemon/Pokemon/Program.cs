﻿using Pokemon.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pokemon
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            PokemonFeu Feunnec = new PokemonFeu("Feunnec",10,2);
            PokemonEau Tiplouf = new PokemonEau("Tiplouf", 10, 2);
            PokemonPlante Germignon = new PokemonPlante("Germignon", 10, 2);

            Dresseur Sacha = new Dresseur("Sacha");
            Sacha.add(Tiplouf);

            Console.WriteLine(Feunnec.ToString());
            Console.WriteLine(Tiplouf.ToString());
            Tiplouf.attaquer(Feunnec);
            Console.WriteLine(Feunnec.ToString());
            Console.WriteLine(Tiplouf.ToString());
            Console.WriteLine(Sacha.ToString());
        }
    }
}
