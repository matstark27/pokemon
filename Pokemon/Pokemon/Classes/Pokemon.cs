﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.Classes
{
    class Pokemon
    {
        private string nom;
        private int hp;
        private int atk;
        private Dresseur d;
        private double exp;
        private int niveau;

        public Pokemon(string unNom, int unHp, int uneAtk)
        {
            this.nom = unNom;
            this.hp = unHp;
            this.atk = uneAtk;
            this.exp = 0;
            this.niveau = 0;
        }

        public string Nom { get => nom; set => nom = value; }
        public int Hp { get => hp; set => hp = value; }
        public int Atk { get => atk; set => atk = value; }

        public Boolean IsDead()
        {
            if (this.hp <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual void attaquer(Pokemon p)
        {
            p.hp = p.hp - (this.atk);
        }

        public override string ToString()
        {
            return "Nom : "+this.nom +" Point de vies : "+ this.hp +" Dégats d'attaques : "+ this.atk+" XP : "+this.exp+" Niveau : "+this.niveau;
        }

    }
}
