﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.Classes
{
    class PokemonElectrique : Pokemon
    {
        public PokemonElectrique(string unNom, int unHp, int uneAtk) : base(unNom, unHp, uneAtk)
        {
            else
            {
                p.Hp = this.Hp - Atk;
            }
             if (p is PokemonVol)
            {
                p.Hp = this.Hp - (Atk * 2);
            }
            else if (p is PokemonEau)
            {
                p.Hp = this.Hp - (Atk * 2);
            }
             else if (p is PokemonPlante)
            {
                p.Hp = this.Hp - (Atk / 2);
            }
             else if (p is PokemonElectrique)
            {
                p.Hp = this.Hp - (Atk / 2);
            }
             else if (p is PokemonDragon)
            {
                p.Hp = this.Hp - (Atk / 2);
            }
        }
    }
}
