﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.Classes
{
    class Dresseur
    {
        string pseudo;
        List<Pokemon> pokemons;
        
        public Dresseur(string unPseudo)
        {
            this.Pseudo = unPseudo;
            this.Pokemons = new List<Pokemon>(3);
        }

        public string Pseudo { get => pseudo; set => pseudo = value; }
        internal List<Pokemon> Pokemons { get => pokemons; set => pokemons = value; }

        public override string ToString()
        {
            return "Pokemon : " + this.Pokemons;
        }
        public void add(Pokemon p)
        {
            pokemons.Add(p);
        }
    }
}
