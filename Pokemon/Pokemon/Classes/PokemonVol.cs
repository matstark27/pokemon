﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.Classes
{
    class PokemonVol : Pokemon
    {
        public PokemonVol(string unNom, int unHp, int uneAtk) : base(unNom, unHp, uneAtk)
        {
            if (p is PokemonPlante)
            {
               
                p.Hp = this.Hp - (Atk*2);
            }
            else
            {
                p.Hp = this.Hp - Atk;
            }
            else if (p is PokemonCombat)
            {
               
                p.Hp = this.Hp - (Atk*2);
            }
            else if (p is PokemonInsecte)
            {
               
                p.Hp = this.Hp - (Atk*2);
            }
            else if (p is PokemonElectrique)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
            else if (p is PokemonRoche)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
        }
    }
}
