﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.Classes
{
    class PokemonFeu : Pokemon
    {
        public PokemonFeu(string unNom, int unHp, int uneAtk) : base(unNom,unHp,uneAtk)
        {

        }

        public override void attaquer(Pokemon p)
        {
            if (p is PokemonPlante)
            {
               
                p.Hp = this.Hp - (Atk*2);
            }
            else if (p is PokemonEau)
            {
                p.Hp = this.Hp - (Atk / 2);
            }
            else
            {
                p.Hp = this.Hp - Atk;
            }
            else if (p is PokemonGlace)
            {
                p.Hp = this.Hp - (Atk*2);
            }
            else if (p is PokemonInsecte)
            {
                p.Hp = this.Hp - (Atk*2);
            }
            else if (p is PokemonFeu)
            {
                p.Hp = this.Hp - (Atk / 2);
            }
            else if (p is PokemonRoche)
            {
                p.Hp = this.Hp - (Atk / 2);
            }
            else if (p is PokemonDragon)
            {
                p.Hp = this.Hp - (Atk / 2);
            }
        }
    }
}
