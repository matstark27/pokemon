﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.Classes
{
    class PokemonInsecte : Pokemon
    {
        public PokemonInsecte(string unNom, int unHp, int uneAtk) : base(unNom, unHp, uneAtk)
        {
            if (p is PokemonPlante)
            {
               
                p.Hp = this.Hp - (Atk*2);
            }
            else
            {
                p.Hp = this.Hp - Atk;
            }
            else if (p is PokemonPsy)
            {
               
                p.Hp = this.Hp - (Atk*2);
            }
            else if (p is PokemonFeu)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
            else if (p is PokemonCombat)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
            else if (p is PokemonPoison)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
            else if (p is PokemonVol)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
            else if (p is PokemonDragon)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
        }
    }
}
