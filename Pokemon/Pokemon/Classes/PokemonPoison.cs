﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon.Classes
{
    class PokemonPoison : Pokemon
    {
        public PokemonPoison(string unNom, int unHp, int uneAtk) : base(unNom, unHp, uneAtk)
        {
            if (p is PokemonPlante)
            {
               
                p.Hp = this.Hp - (Atk*2);
            }
            else
            {
                p.Hp = this.Hp - Atk;
            }
            else if (p is PokemonPoison)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
            else if (p is PokemonSol)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
            else if (p is PokemonSpectre)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
            else if (p is PokemonRoche)
            {
               
                p.Hp = this.Hp - (Atk/2);
            }
        }
    }
}
