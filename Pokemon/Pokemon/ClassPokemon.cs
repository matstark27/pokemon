﻿using System;

namespace Pokemon
{
    public class Pokemon
    {
        private string nom;
        private int hp;
        private int atk;

        public Pokemon(string unNom, int unHp, int uneAtk)
        {
            this.nom = unNom;
            this.hp = unHp;
            this.atk = uneAtk;
        }

        public string Nom { get => nom; set => nom = value; }
        public int Hp { get => hp; set => hp = value; }
        public int Atk { get => atk; set => atk = value; }

        public Boolean IsDead()
        {
            if (hp <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void attaquer(Pokemon p)
        {
            Pokemon(p.hp) = Pokemon(p.hp) - this.atk;
        }

        public override string ToString()
        {
            return (this.nom, this.hp, this.atk);
        }

    }

    public class PokemonFeu : Pokemon
    {
        public PokemonFeu() : base()
        {
        }
        public override void attaquer(Pokemon p)
        {

        }

    }

    public class PokemonEau : Pokemon
    {
        public PokemonEau() : base()
        {
        }
        public override void attaquer(Pokemon p)
        {

        }

    }

    public class PokemonPlante : Pokemon
    {
        public PokemonPlante() : base()
        {
        }
        public override void attaquer(Pokemon p)
        {

        }

    }

}